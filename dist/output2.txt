Criterios Normalizado:
Estilo         0,30 0,29 0,38 
Confiabilidade 0,60 0,57 0,50 
Consumo        0,10 0,14 0,13 

Vetor Criterios:
0,32      0,56      0,12      

Valor Maximo:3,02      CI:0,01      CR:0,02      

Estilo Normalizado:
car1           0,09 0,05 0,29 0,10 
car2           0,36 0,18 0,29 0,15 
car3           0,02 0,05 0,07 0,12 
car4           0,53 0,73 0,36 0,62 

Vetor Estilo:
0,13      0,24      0,07      0,56      

Valor Maximo:4,46      CI:0,15      CR:0,17      

Confiabilidade Normalizado:
car1           0,37 0,52 0,38 0,24 
car2           0,19 0,26 0,23 0,47 
car3           0,07 0,09 0,08 0,06 
car4           0,37 0,13 0,31 0,24 

Vetor Confiabilidade:
0,38      0,29      0,07      0,26      

Valor Maximo:4,19      CI:0,06      CR:0,07      

Consumo Normalizado:
car1           0,09 0,05 0,29 0,10 
car2           0,36 0,18 0,29 0,15 
car3           0,02 0,05 0,07 0,12 
car4           0,53 0,73 0,36 0,62 

Vetor Consumo:
0,13      0,24      0,07      0,56      

Valor Maximo:4,46      CI:0,15      CR:0,17      


Matrizes de entrada:

Criterios:
Estilo         1,00 0,50 3,00 
Confiabilidade 2,00 1,00 4,00 
Consumo        0,33 0,25 1,00 


Estilo:
car1           1,00 0,25 4,00 0,17 
car2           4,00 1,00 4,00 0,25 
car3           0,25 0,25 1,00 0,20 
car4           6,00 4,00 5,00 1,00 


Confiabilidade:
car1           1,00 2,00 5,00 1,00 
car2           0,50 1,00 3,00 2,00 
car3           0,20 0,33 1,00 0,25 
car4           1,00 0,50 4,00 1,00 


Consumo:
car1           1,00 0,25 4,00 0,17 
car2           4,00 1,00 4,00 0,25 
car3           0,25 0,25 1,00 0,20 
car4           6,00 4,00 5,00 1,00 


Vetor Composto:
0,27      0,27      0,07      0,39      

A alternativa escolhida é: car4
