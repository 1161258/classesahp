package LAPR1_Final_Project;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class AproxAHP {

    /**
     * Copia os elementos de uma matriz para outra matriz de destino
     *
     * @param origem matriz de origem
     * @param destino matriz de destino
     */
    public static void copiaMatriz(double[][] origem, double[][] destino) {
        for (int i = 0; i < origem.length; i++) {
            System.arraycopy(origem[i], 0, destino[i], 0, origem[i].length);
        }
    }

    /**
     * Normaliza a matriz ao dividir cada elemento pela soma da coluna
     *
     * @param matriz matriz de entrada desejada para normalizar
     */
    public static void normalizaMatrizes(double[][] matriz) {
        double soma;
        for (int j = 0; j < matriz.length; j++) { //coluna
            soma = somaColuna(matriz, j);
            for (int i = 0; i < matriz.length; i++) { //linha
                matriz[i][j] = matriz[i][j] / soma;
            }
        }
    }

    /**
     * Faz a soma de todos os elementos de cada coluna
     *
     * @param matriz matriz de entrada
     * @param coluna coluna desejada a somar
     * @return
     */
    public static double somaColuna(double[][] matriz, int coluna) {
        double soma = 0;
        for (int i = 0; i < matriz.length; i++) { //linha
            soma += matriz[i][coluna];
        }
        return soma;
    }

    /**
     * Calcula a média de cada linha de uma determinada matriz e retorna os
     * valores atraves de um vetor
     *
     * @param matriz
     * @return
     */
    public static double[] calculaVetorPrioridades(double[][] matriz) {
        double[] vetorPrior = new double[matriz.length];
        double soma;
        for (int i = 0; i < matriz.length; i++) {
            soma = 0;
            for (int j = 0; j < matriz[i].length; j++) {
                soma += matriz[i][j];
            }
            vetorPrior[i] = soma / matriz[i].length;
        }
        return vetorPrior;
    }

    /**
     * Calcula valor proprio maximo
     *
     * @param matriz matriz normalizada de entrada
     * @param vetor vetor de prioridades da matriz de entrada
     * @return
     */
    public static double calcularVMax(double[][] matriz, double[] vetor) {
        double[] vetorMV = Common.multiplicarMV(matriz, vetor);
        //vetorMV vetor resultante da multiplicacao da matriz pelo vetor
        double soma = 0;
        if (vetor.length == vetorMV.length) {
            for (int i = 0; i < vetorMV.length; i++) {
                soma += (vetorMV[i] / vetor[i]);
            }
        }
        return soma / vetor.length;
    }

    /**
     * Metodo para preencher a matriz vMaxCICR com os valores proprios maximos,
     * o indice de consistencia e a razao de consistencia
     *
     * @param vMaxCICR matriz vMaxCICR
     * @param matriz matriz de entrada nao normalizada
     * @param vetor vetor prioridades relativo a matriz nao normalizada
     * @param i posicao para guardar os valores na matriz vMaxCICR
     */
    public static void preencheVMaxCICR(double[][] vMaxCICR, double[][] matriz,
            double[] vetor, int i) {
        vMaxCICR[i][0] = calcularVMax(matriz, vetor);
        vMaxCICR[i][1] = Common.calculaCI(vMaxCICR[i][0], matriz.length);
        vMaxCICR[i][2] = Common.calculaCR(vMaxCICR[i][1], matriz.length);
    }

    /**
     * Calcula o vetor prioridades para cada matriz e introduz os vetores numa
     * matriz de alternativas
     *
     * @param matrizAlt matriz alternativas
     * @param cMatrizC copia matriz criterios
     * @param cMatrizA1 copia normalizada da matriz do criterio 1
     * @param cMatrizA2 copia normalizada da matriz do criterio 2
     * @param cMatrizA3 copia normalizada da matriz do criterio 3
     */
    public static void copiaVetoresAlternativas(double[][] matrizAlt,
            double[][] cMatrizC, double[][] cMatrizA1,
            double[][] cMatrizA2, double[][] cMatrizA3) {

        for (int i = 1; i <= cMatrizC.length; i++) {
            double[] aux;
            switch (i) {
                case 1:
                    aux = calculaVetorPrioridades(cMatrizA1);
                    System.arraycopy(aux, 0, matrizAlt[i - 1], 0, aux.length);
                    break;
                case 2:
                    aux = calculaVetorPrioridades(cMatrizA2);
                    System.arraycopy(aux, 0, matrizAlt[i - 1], 0, aux.length);
                    break;
                case 3:
                    aux = calculaVetorPrioridades(cMatrizA3);
                    System.arraycopy(aux, 0, matrizAlt[i - 1], 0, aux.length);
                    break;
            }
        }
    }

}
