package LAPR1_Final_Project;

import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class LAPR1_Projeto_Final {

    /**
     * Metodo com todos os processos referentes ao método aproximado do calculo
     * do metodo AHP
     *
     * @param nomeFE nome do ficheiro de escrita
     * @param matrizC Matriz criterios
     * @param matrizA1 Matriz alternativas estilo
     * @param matrizA2 Matriz alternativas confiabilidade
     * @param matrizA3 Matriz alternativas consumo
     * @param nomesCA matriz dos nomes de cada criterio e alternativa
     * @throws java.io.FileNotFoundException
     */
    public static void metodoAproximado(String nomeFE, double[][] matrizC,
            double[][] matrizA1, double[][] matrizA2, double[][] matrizA3,
            String[][] nomesCA) throws FileNotFoundException {

        double[][] cMatrizC
                = new double[matrizC.length][matrizC.length];
        double[][] cMatrizA1
                = new double[matrizA1.length][matrizA1.length];
        double[][] cMatrizA2
                = new double[matrizA2.length][matrizA2.length];
        double[][] cMatrizA3
                = new double[matrizA3.length][matrizA3.length];

        AproxAHP.copiaMatriz(matrizC, cMatrizC);
        AproxAHP.copiaMatriz(matrizA1, cMatrizA1);
        AproxAHP.copiaMatriz(matrizA2, cMatrizA2);
        AproxAHP.copiaMatriz(matrizA3, cMatrizA3);

        AproxAHP.normalizaMatrizes(cMatrizC);
        AproxAHP.normalizaMatrizes(cMatrizA1);
        AproxAHP.normalizaMatrizes(cMatrizA2);
        AproxAHP.normalizaMatrizes(cMatrizA3);

        //vetor prioridades dos criterios
        double[] vProprioCriterios
                = AproxAHP.calculaVetorPrioridades(cMatrizC);
        //matriz com os vetores das prioridades de cada matriz
        double[][] matrizAlt
                = new double[cMatrizC.length][cMatrizA1.length];

        AproxAHP.copiaVetoresAlternativas(matrizAlt, cMatrizC,
                cMatrizA1, cMatrizA2, cMatrizA3);

        double[][] vMaxCICR = new double[4][3];

        /* Valor máximo, CI e CR para os criterios*/
        AproxAHP.preencheVMaxCICR(vMaxCICR, matrizC, vProprioCriterios, 0);
        /* Valor máximo, CI e CR para o criterio 1*/
        AproxAHP.preencheVMaxCICR(vMaxCICR, matrizA1, matrizAlt[0], 1);
        /* Valor máximo, CI e CR para a criterio 2*/
        AproxAHP.preencheVMaxCICR(vMaxCICR, matrizA2, matrizAlt[1], 2);
        /* Valor máximo, CI e CR para o criterio 3*/
        AproxAHP.preencheVMaxCICR(vMaxCICR, matrizA3, matrizAlt[2], 3);

        if (vMaxCICR[0][2] > 0.2 || vMaxCICR[1][2] > 0.2
                || vMaxCICR[2][2] > 0.2 || vMaxCICR[3][2] > 0.2) {
            System.out.println("Erro: Matrizes nao consistentes!");
        } else {
            matrizAlt = Common.transporMatriz(matrizAlt);
            //vetor prioridades composta
            double[] vetorComposto
                    = Common.multiplicarMV(matrizAlt, vProprioCriterios);

            matrizAlt = Common.transporMatriz(matrizAlt);

            //escrever para ficheiro todos os campos
            Common.escrever(2, nomeFE, nomesCA, matrizC, matrizA1, matrizA2,
                    matrizA3, vMaxCICR, vetorComposto, vProprioCriterios,
                    matrizAlt, cMatrizC, cMatrizA1, cMatrizA2, cMatrizA3);
        }
    }

    /**
     * Metodo com todos os processos referentes ao método exato do calculo do
     * metodo AHP usando a biblioteca la4j
     *
     * @param nomeFE nome do ficheiro de escrita
     * @param matrizC matriz dos criterios
     * @param matrizA1 matriz do primeiro criterio
     * @param matrizA2 matriz do segundo criterio
     * @param matrizA3 matriz do terceiro criterio
     * @param nomesCA matriz do nome dos criterios e das alternativas
     * @throws FileNotFoundException
     */
    public static void metodoExato(String nomeFE, double[][] matrizC,
            double[][] matrizA1, double[][] matrizA2, double[][] matrizA3,
            String[][] nomesCA) throws FileNotFoundException {

        double[] vetorEValorProprioCriterios
                = ExactAHP.calculoValorEVetorProprio(matrizC);
        double[] vetorEValorProprioEstilo
                = ExactAHP.calculoValorEVetorProprio(matrizA1);
        double[] vetorEValorProprioConfiabilidade
                = ExactAHP.calculoValorEVetorProprio(matrizA2);
        double[] vetorEValorProprioConsumo
                = ExactAHP.calculoValorEVetorProprio(matrizA3);

        double[][] vMaxCICR = new double[4][3];

        /* Valor máximo, CI e CR para os criterios*/
        ExactAHP.preencheVMaxCICR(vMaxCICR, vetorEValorProprioCriterios, matrizC, 0);
        ExactAHP.preencheVMaxCICR(vMaxCICR, vetorEValorProprioEstilo, matrizA1, 1);
        ExactAHP.preencheVMaxCICR(vMaxCICR, vetorEValorProprioConfiabilidade, matrizA2, 2);
        ExactAHP.preencheVMaxCICR(vMaxCICR, vetorEValorProprioConsumo, matrizA3, 3);

        if (vMaxCICR[0][2] > 0.2 || vMaxCICR[1][2] > 0.2
                || vMaxCICR[2][2] > 0.2 || vMaxCICR[3][2] > 0.2) {
            System.out.println("Erro: Matrizes nao consistentes!");
        } else {
            double[] vetorProprioCriterios
                    = ExactAHP.retirarVetorProprio(vetorEValorProprioCriterios);
            double[] vetorProprioEstilo
                    = ExactAHP.retirarVetorProprio(vetorEValorProprioEstilo);
            double[] vetorProprioConfiabilidade
                    = ExactAHP.retirarVetorProprio(vetorEValorProprioConfiabilidade);
            double[] vetorProprioConsumo
                    = ExactAHP.retirarVetorProprio(vetorEValorProprioConsumo);

            double[][] matrizValorAlternativas = new double[3][4];

            matrizValorAlternativas
                    = ExactAHP.juntarVetorEmMatriz(matrizValorAlternativas,
                            vetorProprioEstilo, vetorProprioConfiabilidade,
                            vetorProprioConsumo);
            matrizValorAlternativas
                    = Common.transporMatriz(matrizValorAlternativas);
            double[] vetorResultados
                    = Common.multiplicarMV(matrizValorAlternativas, vetorProprioCriterios);
            Common.escrever(1, nomeFE, nomesCA, matrizC, matrizA1,
                    matrizA2, matrizA3, vMaxCICR, vetorResultados,
                    vetorProprioCriterios, matrizValorAlternativas);

        }
    }

    /**
     * Menu apresentado no ecra para escolha do metodo a ser usado para os
     * calculos
     *
     * @param nomeFE nome do ficheiro de escrita
     * @param matrizC matriz dos criterios
     * @param matrizA1 matriz do criterio 1
     * @param matrizA2 matriz do criterio 2
     * @param matrizA3 matriz do criterio 3
     * @param nomesCA matriz com nomes dos criterios e das alternativas
     * @throws FileNotFoundException
     */
    public static void menu(String nomeFE, double[][] matrizC,
            double[][] matrizA1, double[][] matrizA2, double[][] matrizA3,
            String[][] nomesCA) throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        System.out.printf("Introduza o método desejado:%n"
                + "1 - Exato%n"
                + "2 - Aproximado%n"
                + "3 - Sair%n");
        System.out.printf("Escolha a sua opção: ");
        switch (scan.next()) {
            case "1":
                metodoExato(nomeFE, matrizC, matrizA1, matrizA2, matrizA3,
                        nomesCA);
                break;
            case "2":
                metodoAproximado(nomeFE, matrizC, matrizA1, matrizA2, matrizA3,
                        nomesCA);
                break;
            case "3":
                break;
            default:
                System.out.println("Erro: Input nao reconhecido");
                menu(nomeFE, matrizC, matrizA1, matrizA2, matrizA3, nomesCA);
                break;
        }
    }

    /**
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        String nomeF = args[0]; //nome ficheiro de leitura
        String nomeFE = args[1]; //nome ficheiro de escrita
        double[][] matrizC = new double[3][3]; //matriz criterios
        double[][] matrizA1 = new double[4][4]; //matriz com alternativas do criterio 1 
        double[][] matrizA2 = new double[4][4]; //matriz com alternativas do criterio 2 
        double[][] matrizA3 = new double[4][4]; //matriz com alternativas do criterio 3 
        String[][] nomesCA = new String[2][4]; //matriz com nome dos criterios e das alternativas
        boolean exists = Common.lerFicheiro(nomeF, matrizC, matrizA1, matrizA2,
                matrizA3, nomesCA);

        if (exists) {
            menu(nomeFE, matrizC, matrizA1, matrizA2, matrizA3, nomesCA);
        }
    }
}
