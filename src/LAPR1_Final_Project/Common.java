package LAPR1_Final_Project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class Common {

    /**
     * Matriz RI (Random Consistency Index)
     */
    private static final double[] RI = {
        0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};

    /**
     * Calcula o Indice de Consistencia de uma matriz CI (Consistency Index)
     *
     * @param vMax valor proprio maximo de uma matriz
     * @param ordem ordem da matriz determinada
     * @return
     *
     * ((valor proprio maximo - ordem da matriz) / (ordem da matriz - 1))
     */
    public static double calculaCI(double vMax, int ordem) {
        return (vMax - ordem) / (ordem - 1);
    }

    /**
     * Calcula a Razao de Consistencia de uma matriz CR (Consistency Ratio)
     *
     * @param CI Indice de Consistencia calculado
     * @param ordem Ordem da matriz associada ao IC calculado
     * @return (IC / RI) Em que RI e a tabela de Random Consistency Index
     */
    public static double calculaCR(double CI, int ordem) {
        return (CI / RI[ordem - 1]);
    }

    /**
     * Método usado para multiplicar a matriz contendo os vetores de
     * alternativas pelo vetor proprio da matriz dos critérios
     *
     * @param matriz matriz que contém os vetores de alternativas
     * @param vetor vetor proprio da matriz dos critérios
     * @return vetor resultado da multiplicação
     */
    public static double[] multiplicarMV(double[][] matriz,
            double[] vetor) {

        double soma;
        double[] matRes = new double[matriz.length];
        if (matriz[0].length == vetor.length) {
            for (int i = 0; i < matriz.length; i++) {
                soma = 0;
                for (int j = 0; j < vetor.length; j++) {
                    soma += (matriz[i][j] * vetor[j]);
                }
                matRes[i] = soma;
            }
        }
        return matRes;
    }

    /**
     * Metodo usado para fazer a matriz transposta de uma matriz
     *
     * @param matriz matriz em questão
     * @return matriz transposta da matriz em questao
     */
    public static double[][] transporMatriz(double[][] matriz) {
        double[][] matrizT = new double[matriz[0].length][matriz.length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matrizT[j][i] = matriz[i][j];
            }
        }
        return matrizT;
    }

    /**
     * Apresenta a alternativa melhor de acordo com o vetor composto
     *
     * @param vetor
     * @param matrizNomes
     * @return
     */
    public static String melhorAlternativa(double[] vetor, String[][] matrizNomes) {
        int maiorIndex = 0;
        double maiorDouble = 0;
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] > maiorDouble) {
                maiorDouble = vetor[i];
                maiorIndex = i;
            }
        }
        return "A alternativa escolhida é: " + matrizNomes[1][maiorIndex];
    }

    /**
     * Metodo que le o ficheiro
     *
     * @param nomeF nome do ficheiro a ser lido
     * @param matrizC matriz dos criterios
     * @param matrizA1 matriz das alternativas 1
     * @param matrizA2 matriz das alternativas 2
     * @param matrizA3 matriz das alternativas 3
     * @param nomesCA matriz dos nomes dos criterios e alternativas
     * @return valor booleano correspondente a se o ficheiro existe ou não
     * @throws FileNotFoundException
     */
    public static boolean lerFicheiro(String nomeF, double[][] matrizC,
            double[][] matrizA1, double[][] matrizA2, double[][] matrizA3,
            String[][] nomesCA) throws FileNotFoundException {

        File ficheiro = new File(nomeF);
        if (ficheiro.exists()) {
            Scanner lerF = new Scanner(ficheiro);
            int nLinha = -1; //numero de linhas lido
            int matrizesLidas = 0;
            int check = 0;

            while (lerF.hasNext()) {
                //corta a linha e separa elementos
                String[] linha = lerF.nextLine().split(" +");
                //se a linha tiver pelo menos 1 coluna
                if (linha.length > 1) {
                    if (nLinha == -1) {//se ainda nao tiver lido o cabecalho
                        matrizesLidas++; //conta que uma linha foi lida
                        if (check == 0) {
                            for (int i = 1; i < linha.length; i++) {
                                nomesCA[check][i - 1] = linha[i];
                            }
                            check++;
                        }
                        if (check == 1) {
                            for (int i = 1; i < linha.length; i++) {
                                nomesCA[check][i - 1] = linha[i];
                            }
                        }
                    }
                    if (nLinha >= 0) { //depois de já ter lido o cabecalho
                        if (matrizesLidas == 1) {
                            trataMatrizes(matrizC, linha, matrizC.length, nLinha);
                        }
                        if (matrizesLidas == 2) {
                            trataMatrizes(matrizA1, linha, matrizA1.length, nLinha);
                        }
                        if (matrizesLidas == 3) {
                            trataMatrizes(matrizA2, linha, matrizA2.length, nLinha);
                        }
                        if (matrizesLidas == 4) {
                            trataMatrizes(matrizA3, linha, matrizA3.length, nLinha);
                        }
                    }
                    nLinha++;
                } else {
                    //se encontrar uma linha em branco prepara para ler cabecalho
                    nLinha = -1;
                }
            }
        } else {
            System.out.println("Ficheiro não encontrado!");
            return false;
        }
        return true;
    }

    /**
     * Metodo que insere os valores nas matrizes correspondentes
     *
     * @param matriz //matriz definida
     * @param linha //linha obtida do ficheiro
     * @param ordem //ordem da matriz
     * @param nLinha //numero de linhas lido
     */
    private static void trataMatrizes(double[][] matriz, String[] linha,
            int ordem, int nLinha) {
        for (int i = 0; i <= ordem - 1; i++) {
            try {
                matriz[nLinha][i] = Double.parseDouble(linha[i]);
                //se conseguir converter insere na matriz

            } catch (NumberFormatException e) {
                //senao tenta converter num array e faz o calculo e tenta colocar na matriz
                String[] frac = linha[i].split("/");
                matriz[nLinha][i]
                        = (Double.parseDouble(frac[0])
                        / Double.parseDouble(frac[1]));
            }
        }
    }

    /**
     * Escreve toda a informacao para um ficheiro determinado pelo utilizador
     * com as matrizes normalizadas
     *
     * @param op opcao escolhida pelo utilizador
     * @param nomeFE nome do ficheiro introduzido pelo utilizador
     * @param matrizC matriz criterios
     * @param matrizA1 matriz alternativas criterio 1
     * @param matrizA2 matriz alternativas criterio 2
     * @param matrizA3 matriz alternativas criterio 3
     * @param vMaxCICR valores maximos, ci e cr para todas as matrizes
     * @param priComp vetor final das alternativas prioritarias
     * @param cMatrizC copia da matriz criterios normalizada
     * @param priC vetor proprio dos criterios
     * @param matrizAlt matriz com vetores prioridades das matrizes de cada
     * criterio
     * @param cMatrizA1 copia da matriz criterio 1 normalizada
     * @param cMatrizA2 copia da matriz criterio 2 normalizada
     * @param cMatrizA3 copia da matriz criterio 3 normalizada
     * @param carAlt nomes dos criterios e das alternativas
     * @throws FileNotFoundException
     */
    public static void escrever(int op, String nomeFE, String[][] carAlt,
            double[][] matrizC, double[][] matrizA1, double[][] matrizA2,
            double[][] matrizA3, double[][] vMaxCICR, double[] priComp,
            double[] priC, double[][] matrizAlt, double[][] cMatrizC,
            double[][] cMatrizA1, double[][] cMatrizA2, double[][] cMatrizA3)
            throws FileNotFoundException {

        try (Formatter write = new Formatter(new File(nomeFE))) {
            System.out.println("Matrizes de entrada:");
            System.out.println("Criterios");
            apresentaMatrizes(matrizC);
            System.out.println(carAlt[0][0]);
            apresentaMatrizes(matrizA1);
            System.out.println(carAlt[0][1]);
            apresentaMatrizes(matrizA2);
            System.out.println(carAlt[0][2]);
            apresentaMatrizes(matrizA3);
            
            if (op == 2) {
                writerM(write, "Criterios Normalizado", cMatrizC, carAlt, 0);
                writerV(write, "Vetor Criterios", priC);
                writerT(write, 0, vMaxCICR);

                writerM(write, carAlt[0][0] + " Normalizado", cMatrizA1,
                        carAlt, 1);
                writerV(write, "Vetor " + carAlt[0][0], matrizAlt[0]);
                writerT(write, 1, vMaxCICR);

                writerM(write, carAlt[0][1] + " Normalizado", cMatrizA2,
                        carAlt, 1);
                writerV(write, "Vetor " + carAlt[0][1], matrizAlt[1]);
                writerT(write, 2, vMaxCICR);

                writerM(write, carAlt[0][2] + " Normalizado", cMatrizA3, carAlt, 1);
                writerV(write, "Vetor " + carAlt[0][2], matrizAlt[2]);
                writerT(write, 3, vMaxCICR);

            } else {
                writerV(write, "Vetor Criterios", priC);
                writerT(write, 0, vMaxCICR);

                writerV(write, "Vetor " + carAlt[0][0], matrizAlt[0]);
                writerT(write, 1, vMaxCICR);

                writerV(write, "Vetor " + carAlt[0][1], matrizAlt[1]);
                writerT(write, 2, vMaxCICR);

                writerV(write, "Vetor " + carAlt[0][2], matrizAlt[2]);
                writerT(write, 3, vMaxCICR);
            }
            write.format("%nMatrizes de entrada:%n%n");
            writerM(write, "Criterios", matrizC, carAlt, 0);
            write.format("%n");
            writerM(write, carAlt[0][0], matrizA1, carAlt, 1);
            write.format("%n");
            writerM(write, carAlt[0][1], matrizA2, carAlt, 1);
            write.format("%n");
            writerM(write, carAlt[0][2], matrizA3, carAlt, 1);
            write.format("%nVetor Composto:%n");
            System.out.println("Vetor Composto:");
            for (int i = 0; i < priComp.length; i++) {
                write.format("%-10.2f", priComp[i]);
                System.out.printf("%-10.2f", priComp[i]);
            }
            System.out.printf("%n");
            write.format("%n");

            System.out.printf("%n%s%n",melhorAlternativa(priComp, carAlt));
            write.format("%n%s%n",melhorAlternativa(priComp, carAlt));
        }

    }
    
    /**
     * Metodo que pode ser chamado quando as matrizes normalizadas nao estao
     * disponiveis ou nao foram calculadas
     *
     * @param op opcao escolhida pelo utilizador
     * @param nomeFE nome do ficheiro introduzido pelo utilizador
     * @param matrizC matriz criterios
     * @param matrizA1 matriz alternativas criterio 1
     * @param matrizA2 matriz alternativas criterio 2
     * @param matrizA3 matriz alternativas criterio 3
     * @param vMaxCICR valores maximos, ci e cr para todas as matrizes
     * @param priComp vetor final das alternativas prioritarias
     * @param carAlt nomes dos criterios e das alternativas
     * @param priC vetor proprio dos criterios
     * @param matrizAlt matriz com vetores prioridades das matrizes de cada
     * criterio
     * @throws FileNotFoundException
     */
    public static void escrever(int op, String nomeFE, String[][] carAlt,
            double[][] matrizC, double[][] matrizA1, double[][] matrizA2,
            double[][] matrizA3, double[][] vMaxCICR, double[] priComp,
            double[] priC, double[][] matrizAlt)
            throws FileNotFoundException {

        double[][] nullMatrix = new double[0][0];

        escrever(op, nomeFE, carAlt, matrizC, matrizA1, matrizA2, matrizA3,
                vMaxCICR, priComp, priC, matrizAlt, nullMatrix, nullMatrix,
                nullMatrix, nullMatrix);
    }

    /**
     * Escreve os valores proprios maximos, e os calculos do indice de
     * consistencia e a razao de consistencia
     *
     * @param write objeto formatter
     * @param linha linha da matriz dos valores a ler
     * @param matriz matriz dos valores maximos, CI e CR
     */
    private static void writerT(Formatter write, int linha, double[][] matriz) {
        write.format("%n%nValor Maximo:%-10.2f", matriz[linha][0]);
        write.format("CI:%-10.2f", matriz[linha][1]);
        write.format("CR:%-10.2f%n%n", matriz[linha][2]);
    }

    /**
     * Escreve para ficheiro uma matriz determinada bem como o nome da mesma
     *
     * @param write objeto formatter
     * @param nome nome da matriz
     * @param matriz matriz com valores normalizados
     * @param carAlt matriz com os nomes das alternativas e dos criterios
     * @param nLinhaNomes numero da linha a ser lida da matriz carAlt
     */
    private static void writerM(Formatter write, String nome, double[][] matriz,
            String[][] carAlt, int nLinhaNomes) {
        write.format("%s:%n", nome);
        for (int i = 0; i < matriz.length; i++) {
            write.format("%-15s", carAlt[nLinhaNomes][i]);
            for (int j = 0; j < matriz.length; j++) {
                write.format("%-5.2f", matriz[i][j]);
            }
            write.format("%n");
        }
        write.format("%n");
    }

    /**
     * Escreve para ficheiro um vetor determinado bem como o nome do mesmo
     *
     * @param write objeto formatter
     * @param nome nome da matriz
     * @param vetor vetor proprio de uma matriz
     */
    private static void writerV(Formatter write, String nome, double[] vetor) {
        write.format("%s:%n", nome);
        for (int i = 0; i < vetor.length; i++) {
            write.format("%-10.2f", vetor[i]);
        }
    }

    /**
     * Apresenta a matriz desejada
     *
     * @param matriz
     */
    private static void apresentaMatrizes(double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for(int j = 0; j< matriz.length; j++){
                System.out.printf("%-10.2f", matriz[i][j]);
            }
            System.out.printf("%n");
        }
        System.out.println("");
    }
    
}
